"""
Copyright 2016 Dmitriy Vasilyev

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
"""

import os
import json
from django.utils.crypto import get_random_string

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

SECRET = os.path.join(BASE_DIR, 'settings/secret.json')
DATABASE = os.path.join(BASE_DIR, 'settings/database.json')

default_secret = {
    'secret_key': 'default~!'
}

default_database = {
    'ENGINE':   'django.db.backends.sqlite3',
    'NAME':     os.path.join(BASE_DIR, 'db.sqlite3'),
}


def secret_key():

    try:
        with open(SECRET, 'r') as handle:
            return json.load(handle)['secret_key']

    except (OSError, json.JSONDecodeError, KeyError):
        return default_secret


def database():

    try:
        with open(DATABASE, 'r') as handle:
            return json.load(handle)
    except OSError:
        return default_database


def generator():
    chars = 'abcdefghijklmnopqrstuvwxyz0123456789!@#$%^&*(-_=+)'
    return {'secret_key': get_random_string(50, chars), }


if __name__ == '__main__':

    secret = json.dumps(generator())

    with open(SECRET, 'w') as f:
        f.write(secret)

    if not os.path.isfile(DATABASE):

        database = json.dumps(default_database)

        with open(DATABASE, 'w') as f:
            f.write(database)
