"""
Copyright 2016 Dmitriy Vasilyev

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
"""

from django.contrib import admin
from django.contrib.auth.models import Group
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from django.contrib.auth.admin import GroupAdmin

from .models import Articles, User, ProxyGroup


class ArticlesAdmin(admin.ModelAdmin):
    fields = ['title', 'text', ]


class UserAdmin(BaseUserAdmin):
    fieldsets = BaseUserAdmin.fieldsets
    fieldsets[1][1]['fields'] = ('first_name', 'last_name', 'middle_name', 'email', 'department', )

admin.site.unregister(Group)
admin.site.register(Articles, ArticlesAdmin)
admin.site.register(User, UserAdmin)
admin.site.register(ProxyGroup, GroupAdmin)
