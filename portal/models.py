"""
Copyright 2016 Dmitriy Vasilyev

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
"""

from django import forms
from django.db import models
from django.contrib.auth.models import AbstractUser, Group
from django.contrib.auth.forms import PasswordChangeForm
from django.utils.translation import ugettext_lazy as _


class Articles(models.Model):
    """
    Статьи
    """

    LABELS = {
        'title': 'Заголовок',
        'text': 'Текст',
    }

    class Meta:
        verbose_name = 'Статьи'
        verbose_name_plural = 'Статьи'

    title = models.CharField(max_length=255, verbose_name=LABELS['title'])
    text = models.TextField(verbose_name=LABELS['text'])
    date = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.title


class User(AbstractUser):
    """
    Расширение модели пользователя
    """

    LABELS = {
        'middle_name': 'Отчество',
        'department': 'Отдел',
    }

    DEPARTMENTS = (
        (1, 'АСУ'),
        (2, 'ПТС'),
        (3, 'Электроинспекция'),
    )

    middle_name = models.CharField(
        max_length=30,
        blank=True,
        null=True,
        verbose_name='Отчество')

    department = models.SmallIntegerField(
        choices=DEPARTMENTS,
        blank=True,
        null=True,
        verbose_name=LABELS['department'])


class ProxyGroup(Group):
    """
    Прокси модель группы
    """

    class Meta:
        proxy = True
        verbose_name = _('group')
        verbose_name_plural = _('groups')


class LoginForm(forms.Form):
    """
    Форма авторизации
    """

    LABELS = {
        'username': 'Имя пользователя',
        'password': 'Пароль'
    }

    username = forms.CharField(
        label=LABELS['username'],
        max_length=255,
        widget=forms.TextInput(attrs={
            'class': 'form-control',
        }), required=True
    )

    password = forms.CharField(
        label=LABELS['password'],
        max_length=255,
        widget=forms.PasswordInput(attrs={
            'class': 'form-control',
        }), required=True
    )


class ChangePasswordForm(PasswordChangeForm):
    """
    Форма смены паролей
    """

    def __init__(self, *args, **kwargs):
        super(ChangePasswordForm, self).__init__(*args, **kwargs)

        self.fields['old_password'].widget.attrs.update({
            'class': 'form-control',
        })
        self.fields['new_password1'].widget.attrs.update({
            'class': 'form-control',
        })

        self.fields['new_password2'].widget.attrs.update({
            'class': 'form-control',
        })

        self.fields['new_password2'].help_text = \
            'Для подтверждения введите, пожалуйста, пароль ещё раз.'
