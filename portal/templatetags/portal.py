from django import template

register = template.Library()


@register.filter()
def field_type(field):
    """
    Фильтр который позволяет получить тип поля формы
    """

    return field.field.widget.__class__.__name__
