"""
Copyright 2016 Dmitriy Vasilyev

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
"""

from django.conf import settings
from django.http import Http404
from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.contrib import messages

from .models import Articles
from .models import LoginForm, ChangePasswordForm


def articles(request, article):
    """
    Статьи
    """

    try:
        content = Articles.objects.get(pk=article)
    except Articles.DoesNotExist:
        raise Http404

    return render(request, 'article.html', {
        'title': content.title,
        'text': content.text,
    })


def sign_in(request):
    """
    Страница авторизации
    """

    title = 'Авторизация'

    if request.POST:
        form = LoginForm(request.POST)

        if form.is_valid():
            user = authenticate(
                username=form.cleaned_data['username'],
                password=form.cleaned_data['password'],
            )

            if user is not None:

                if user.is_active:

                    login(request, user)

                    if 'next' in request.GET:
                        return redirect(request.GET.get('next'))
                    else:
                        return redirect(settings.HOME_URL)

                else:
                    messages.error(request, 'Пользователь неактивен')
            else:
                messages.error(request, 'Неправильное имя пользователя или пароль')

    else:
        form = LoginForm()

    return render(request, 'forms.html', {
        'title': title,
        'forms': (form,),
        'submit': 'Войти',
    })


def sign_out(request):
    """
    Выход
    """
    logout(request)
    return redirect(settings.LOGIN_URL)


@login_required()
def change_password(request):
    """
    Смена пароля
    """

    title = 'Смена пароля'

    if request.POST:
        form = ChangePasswordForm(data=request.POST, user=request.user)

        if form.is_valid():
            form.save()
            messages.success(request, 'Пароль обновлён')
            logout(request)
            return redirect(settings.LOGIN_URL)

    else:
        form = ChangePasswordForm(user=request.user)

    return render(request, 'forms.html', {
        'title': title,
        'forms': (form,),
        'submit': 'Изменить'
    })
