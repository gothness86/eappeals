"""
Copyright 2016 Dmitriy Vasilyev

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
"""

from django.conf.urls import include, url

from . import views
from .api import urls

urlpatterns = [
    url(r'^create-appeal/$', views.create_appeal, name='create_appeal'),
    url(r'^appeals-list/$', views.appeals_list, name='appeals_list'),
    url(r'^appeal-details/$', views.appeal_details, name='appeal_details'),
    url(r'^appeals-journal/$', views.appeals_journal, name='appeals_journal'),
    url(r'^appeals-form9/$', views.appeals_form9, name='appeals_form9'),
    # url(r'^api/', include(urls)),
]
