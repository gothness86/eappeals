"""
Copyright 2016 Dmitriy Vasilyev

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
"""
from importlib import import_module
from time import strftime
from datetime import date, timedelta

from django import forms
from django.db import models
from django.conf import settings


class Appeals(models.Model):
    """
    Обращения
    """

    LABELS = {
        'id':               '№',
        'date':             'Дата обращения',
        'time':             'Время обращения',
        'com_method':       'Способ обращения',
        'appeal_type':      'Тип обращения',
        'client_type':      'Вид потребителя',
        'legal_name':       'Офиц. наименование',
        'client':           'Контактное лицо',
        'com_info':         'Контактная информация',
        'object_name':      'Объект',
        'request':          'Содержит заявку',
        'finished_events':  'Выполненные мероприятия',
        'planned_events':   'Планируемые мероприятия',
    }

    COM_METHOD = (
        (1, 'В письменной форме'),
        (2, 'Через интернет'),
        (3, 'По телефону'),
        (4, 'Личное обращение'),
        (0, 'Прочее'),
    )

    APPEAL_TYPES = (
        (1, 'Оказание услуг по передаче электрической энергии'),
        (2, 'Осуществление технологического присоединения'),
        (3, 'Коммерческий учет электрической энергии'),
        (4, 'Качество обслуживания потребителей'),
        (5, 'Техническое обслуживание электросетевых объектов'),
        (0, 'Прочее'),
    )

    CLIENT_TYPES = (
        (1, 'Физическое лицо'),
        (2, 'Индивидуальный предприниматель'),
        (3, 'Юридическое лицо'),
    )

    REQUEST = (
        (0, 'не содержит'),
        (1, 'по технологическому присоединению'),
        (2, 'организация коммерческого учёта'),
    )

    class Meta:
        db_table = 'appeals'
        verbose_name = 'Обращения'
        verbose_name_plural = 'Обращения'

    date = models.DateField(verbose_name=LABELS['date'])
    time = models.TimeField(verbose_name=LABELS['time'])
    com_method = models.SmallIntegerField(choices=COM_METHOD, verbose_name=LABELS['com_method'])
    appeal_type = models.SmallIntegerField(choices=APPEAL_TYPES, verbose_name=LABELS['appeal_type'])
    client_type = models.SmallIntegerField(choices=CLIENT_TYPES, verbose_name=LABELS['client_type'])
    legal_name = models.CharField(max_length=255, null=True, blank=True, verbose_name=LABELS['legal_name'])
    client = models.CharField(max_length=255, null=True, blank=True, verbose_name=LABELS['client'])
    com_info = models.CharField(max_length=255, null=True, blank=True, verbose_name=LABELS['com_info'])
    object_name = models.CharField(max_length=255, null=True, blank=True, verbose_name=LABELS['object_name'])
    request = models.SmallIntegerField(choices=REQUEST, verbose_name=LABELS['request'])
    finished_events = models.TextField(null=True, blank=True, verbose_name=LABELS['finished_events'])
    planned_events = models.TextField(null=True, blank=True, verbose_name=LABELS['planned_events'])
    user = models.ForeignKey(settings.AUTH_USER_MODEL)

    def __str__(self):
        return "%06d" % self.pk


class AppealsForm(forms.ModelForm):
    """
    Форма добавления обращения
    """

    HELP_TEXTS = {
        'date':     'Введите дату в формате ДД.ММ.ГГГГ',
        'time':     'Введите время в формате ЧЧ:ММ',
        'com_info': 'Введите контактную информацию (номер телефона и т.п.)'
    }

    DATE_FORMATS = (
        '%d.%m.%Y',
    )

    TIME_FORMATS = (
        '%H:%M',
    )

    class Meta:
        model = Appeals
        fields = [
            'date', 'time',
            'com_method', 'appeal_type',
            'client_type', 'legal_name', 'client', 'com_info', 'object_name',
            'request', 'finished_events', 'planned_events',
        ]

    date = forms.DateField(
        label=Appeals.LABELS['date'],
        help_text=HELP_TEXTS['date'],
        input_formats=DATE_FORMATS,
        initial=lambda: strftime(AppealsForm.DATE_FORMATS[0]),
        widget=forms.TextInput(attrs={
            'class': 'form-control',
            'autocomplete': 'off',
        }), required=True
    )

    time = forms.TimeField(
        label=Appeals.LABELS['time'],
        help_text=HELP_TEXTS['time'],
        input_formats=TIME_FORMATS,
        initial=lambda: strftime(AppealsForm.TIME_FORMATS[0]),
        widget=forms.TextInput(attrs={
            'class': 'form-control',
            'autocomplete': 'off',
        }), required=True
    )

    com_method = forms.ChoiceField(
        label=Appeals.LABELS['com_method'],
        choices=Appeals.COM_METHOD,
        widget=forms.Select(attrs={
            'class': 'form-control selectpicker',
        }), required=True
    )

    appeal_type = forms.ChoiceField(
        label=Appeals.LABELS['appeal_type'],
        choices=Appeals.APPEAL_TYPES,
        widget=forms.Select(attrs={
            'class': 'form-control selectpicker',
        }), required=True
    )

    client_type = forms.ChoiceField(
        label=Appeals.LABELS['client_type'],
        choices=Appeals.CLIENT_TYPES,
        widget=forms.Select(attrs={
            'class': 'form-control selectpicker',
        }), required=True
    )

    legal_name = forms.CharField(
        label=Appeals.LABELS['legal_name'],
        max_length=255,
        widget=forms.TextInput(attrs={
            'class': 'form-control',
        }), required=False
    )

    client = forms.CharField(
        label=Appeals.LABELS['client'],
        max_length=255,
        widget=forms.TextInput(attrs={
            'class': 'form-control',
        }), required=False
    )

    com_info = forms.CharField(
        label=Appeals.LABELS['com_info'],
        help_text=HELP_TEXTS['com_info'],
        max_length=255,
        widget=forms.TextInput(attrs={
            'class': 'form-control',
        }), required=False
    )

    object_name = forms.CharField(
        label=Appeals.LABELS['object_name'],
        max_length=255,
        widget=forms.TextInput(attrs={
            'class': 'form-control',
        }), required=False
    )

    request = forms.ChoiceField(
        label=Appeals.LABELS['request'],
        choices=Appeals.REQUEST,
        widget=forms.Select(attrs={
            'class': 'form-control selectpicker',
        }), required=True
    )

    finished_events = forms.CharField(
        label=Appeals.LABELS['finished_events'],
        widget=forms.Textarea(attrs={
            'class': 'form-control',
            'rows': 3,
        }), required=True
    )

    planned_events = forms.CharField(
        label=Appeals.LABELS['planned_events'],
        widget=forms.Textarea(attrs={
            'class': 'form-control',
            'rows': 3,
        }), required=False
    )

    def __init__(self, user, *args, **kwargs):
        self.user = user
        super(AppealsForm, self).__init__(*args, **kwargs)

    def save(self, commit=True):
        model = super(AppealsForm, self).save(commit=False)
        model.user = self.user
        if commit:
            model.save()
        return model


class AppealsReportForm(forms.Form):
    """
    Форма для отчётов по обращениям
    """

    LABELS = {
        'start_date':   'Начало периода',
        'end_date':     'Конец периода',
    }

    HELP_TEXTS = {
        'start_date':   'Введите дату в формате ДД.ММ.ГГГГ',
        'end_date':     'Введите дату в формате ДД.ММ.ГГГГ',
    }

    DATE_FORMATS = (
        '%d.%m.%Y',
    )

    start_date = forms.DateField(
        label=LABELS['start_date'],
        help_text=HELP_TEXTS['start_date'],
        input_formats=DATE_FORMATS,
        initial=lambda: AppealsReportForm._init_start_date(),
        widget=forms.TextInput(attrs={
            'class': 'form-control',
            'autocomplete': 'off',
        }), required=True
    )

    end_date = forms.DateField(
        label=LABELS['end_date'],
        help_text=HELP_TEXTS['end_date'],
        input_formats=DATE_FORMATS,
        initial=lambda: AppealsReportForm._init_end_date(),
        widget=forms.TextInput(attrs={
            'class': 'form-control',
            'autocomplete': 'off',
        }), required=True
    )

    @staticmethod
    def _init_start_date():
        previous_month = (
            date.today().replace(day=1) - timedelta(days=1)
        ).replace(day=1)
        return previous_month.strftime(AppealsReportForm.DATE_FORMATS[0])

    @staticmethod
    def _init_end_date():
        current_month = date.today().replace(day=1)
        return current_month.strftime(AppealsReportForm.DATE_FORMATS[0])


class AppealsReportFormWithDepartment(AppealsReportForm):
    """
    Форма для отчётов по обращениям с привязкой к отделу
    """

    LABELS = AppealsReportForm.LABELS.copy()
    LABELS.update({
        'department': 'Отдел',
    })

    DEPARTMENTS = (
        ('all', 'Все'),
        (2, 'ПТС'),
        (3, 'Электроинспекция')
    )

    department = forms.ChoiceField(
        label=LABELS['department'],
        choices=DEPARTMENTS,
        widget=forms.Select(attrs={
            'class': 'form-control selectpicker',
        }), required=True
    )