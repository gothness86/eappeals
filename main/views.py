"""
Copyright 2016 Dmitriy Vasilyev

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
"""

from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.db.models import Q
from django.http import HttpResponse, Http404
from django.shortcuts import render, redirect
from django.template import loader

from .models import Appeals, AppealsForm, AppealsReportForm, AppealsReportFormWithDepartment


@login_required()
def create_appeal(request):
    """
    Добавление обращения
    """

    title = 'Регистрация обращений'

    if request.POST:
        form = AppealsForm(user=request.user, data=request.POST)

        if form.is_valid():
            reg_number = str(form.save())
            messages.success(request, "Обращение № %s зарегистрировано" % reg_number)
            return redirect(create_appeal)
    else:
        form = AppealsForm(user=request.user)

    return render(request, 'main/create_appeal.html', {
        'title': title,
        'forms': (form,),
        'submit': 'Отправить',
    })


@login_required()
def appeals_list(request):
    """
    Отображение и поиск обращений
    """

    title = 'Просмотр и поиск обращений'

    data = Appeals.objects.all().order_by('-id')

    if 'q' in request.GET:
        data = data.filter(
            Q(client__icontains=request.GET.get('q')) |
            Q(legal_name__icontains=request.GET.get('q')) |
            Q(object_name__contains=request.GET.get('q'))
        )

    paginator = Paginator(data, 30)

    if 'page' in request.GET:
        try:
            items = paginator.page(request.GET.get('page'))
        except PageNotAnInteger:
            items = paginator.page(1)
        except EmptyPage:
            items = paginator.page(paginator.num_pages)
    else:
        items = paginator.page(1)

    return render(request, 'main/appeals_list.html', {
        'title': title,
        'items': items,
    })


@login_required()
def appeal_details(request):
    """
    Отображение деталей по обращениям
    """

    if 'id' not in request.GET:
        raise Http404

    try:
        data = Appeals.objects.get(pk=request.GET.get('id'))
    except Appeals.DoesNotExist:
        raise Http404

    return render(request, 'main/appeal_details.html', {
        'data': data,
    })


@login_required()
def appeals_journal(request):
    """
    Журнал учёта обращений
    """

    title = 'Журнал учёта обращений потребителей'

    if request.POST:

        form = AppealsReportForm(request.POST)

        if form.is_valid():

            response = HttpResponse(content_type='text/xml')
            response['Content-Disposition'] = 'attachment; filename="appeals_journal.xml"'
            pattern = loader.get_template('main/appeals_journal.xml')

            items = Appeals.objects.filter(
                date__gte=form.cleaned_data.get('start_date'),
                date__lte=form.cleaned_data.get('end_date')
            ).order_by('id').select_related('user')

            response.write(pattern.render({
                'items': items,
            }))

            return response

    else:
        form = AppealsReportForm()

    return render(request, 'main/appeals_journal.html', {
        'title': title,
        'forms': (form, ),
        'submit': 'Сформировать'
    })


@login_required()
def appeals_form9(request):
    """
    Отчёт по обращениям по форме №9
    """

    title = 'Информация по обращениям потребителей (отчёт по форме №9)'

    if request.POST:

        form = AppealsReportFormWithDepartment(request.POST)

        if form.is_valid():

            response = HttpResponse(content_type='text/xml')
            response['Content-Disposition'] = 'attachment; filename="appeals_form9.xml"'
            pattern = loader.get_template('main/appeals_form9.xml')

            items = Appeals.objects.filter(
                date__gte=form.cleaned_data.get('start_date'),
                date__lte=form.cleaned_data.get('end_date')
            ).order_by('id')

            if form.cleaned_data.get('department') != 'all':
                items = items.filter(
                    user__department=form.cleaned_data.get('department')
                )

            response.write(pattern.render({
                'items': items,
            }))

            return response

    else:
        form = AppealsReportFormWithDepartment()

    return render(request, 'main/appeals_form9.html', {
        'title': title,
        'forms': (form, ),
        'submit': 'Сформировать'
    })
