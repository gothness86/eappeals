"""
Copyright 2016 Dmitriy Vasilyev

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
"""

from setuptools import setup, find_packages

setup(
    name='eAppeals',
    version='1.0.0',
    description='Registration of appeals',
    author='Dmitriy Vasilyev',
    namespace_packages=['home'],
    packages=find_packages(),
    platforms='any',
    zip_safe=False,
    include_package_data=True,
    install_requires=[
        'Django==1.10.3',
        'psycopg2==2.6.2',
        'djangorestframework==3.5.3',
    ],
    classifiers=[
        'Development Status :: 4 - Beta',
        'Environment :: Web Environment',
        'License :: OSI Approved :: Apache Software License',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Programming Language :: Python:: 3.5',
        'Natural Language :: Russian'
        'Topic :: Internet :: WWW/HTTP :: Dynamic Content',
        'Framework :: Django',
    ],
)