"""
Copyright 2016 Dmitriy Vasilyev

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
"""

from django.conf import settings
from django.conf.urls import include, url
from django.contrib import admin
from django.views.generic import RedirectView, TemplateView
from django.conf.urls.static import static

import portal.urls
import main.urls

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^portal/', include(portal.urls)),
    url(r'^main/', include(main.urls)),
    url(r'^404/', TemplateView.as_view(template_name='404.html')),
    url(r'^$', RedirectView.as_view(url='/portal/', permanent=True)),
]

if settings.DEBUG:
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)

